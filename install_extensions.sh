#!/bin/bash
code --install-extension Angular.ng-template
code --install-extension CoenraadS.bracket-pair-colorizer
code --install-extension Mikael.Angular-BeastCode
code --install-extension Zignd.html-css-class-completion
code --install-extension christian-kohler.path-intellisense
code --install-extension eg2.tslint
code --install-extension infinity1207.angular2-switcher
code --install-extension johnpapa.Angular2
code --install-extension ms-vscode.sublime-keybindings
code --install-extension msjsdiag.debugger-for-chrome
code --install-extension rbbit.typescript-hero
code --install-extension robertohuertasm.vscode-icons
code --install-extension steoates.autoimport
