import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonListComponent } from './persons/person-list/person-list.component';
import { PersonComponent } from './persons/person/person.component';

const routes: Routes = [
  { path: 'person/list', component: PersonListComponent },
  { path: 'person/view/:id', component: PersonComponent },
  { path: '', pathMatch: 'full', redirectTo: 'person/list' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
