import { Injectable } from '@angular/core';
import { Person } from './person';

@Injectable()
export class PersonService {
  private data: { [id: number]: Person } = {
    1: { id: 1, firstName: 'first1', lastName: 'last1' },
    2: { id: 2, firstName: 'first2', lastName: 'last2' },
    3: { id: 3, firstName: 'first3', lastName: 'last3' },
    4: { id: 4, firstName: 'first4', lastName: 'last4' },
  };
  constructor() { }

  findAll(): Person[] {
    return Object.values(this.data);
  }

  find(id: number) {
    return this.data[id];
  }

}
