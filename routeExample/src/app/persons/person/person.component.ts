import { PersonService } from './../shared/person.service';
import { Person } from './../shared/person';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  person: Person;
  constructor(
    private route: ActivatedRoute,
    private service: PersonService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => this.person = this.service.find(+params.get('id'))
    );
  }

}
