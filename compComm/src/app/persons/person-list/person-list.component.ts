import { PersonService } from './../shared/person.service';
import { Component, OnInit } from '@angular/core';
import { Person } from '../shared/person';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  persons: Person[];
  selected: Person = new Person;
  constructor(private personService: PersonService) { }

  ngOnInit() {
    this.persons = this.personService.findAll();
  }

  selectPerson(person: Person) {
    this.selected = person;
  }

}
