import { PersonService } from './../shared/person.service';
import { Component, OnInit } from '@angular/core';
import { Person } from '../shared/person';

@Component({
  selector: 'app-mylist',
  templateUrl: './mylist.component.html',
  styleUrls: ['./mylist.component.css']
})
export class MylistComponent implements OnInit {
  numSelected = 0;
  persons = [];
  constructor(private personService: PersonService) { }

  ngOnInit() {
    this.persons = this.personService.findAll();
  }

  childSelectionChanged(value: boolean) {
    if (value) {
      this.numSelected++;
    } else {
      this.numSelected--;
    }
  }

}
