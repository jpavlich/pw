import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../shared/person';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  @Input() person: Person;
  @Output() selectionChangedEvent = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  onChange(event) {
    this.selectionChangedEvent.emit(event.target.checked);
  }

}
