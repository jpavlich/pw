import { Injectable } from '@angular/core';
import { Person } from './person';

@Injectable()
export class PersonService {
  private data: { [id: number]: Person } = {
    1: {firstName: 'first1', lastName: 'last1'},
    2: {firstName: 'first2', lastName: 'last2'},
    3: {firstName: 'first3', lastName: 'last3'},
    4: {firstName: 'first4', lastName: 'last4'},
    5: {firstName: 'first5', lastName: 'last5'}
  };
  constructor() { }

  findAll() {
    return Object.values(this.data);
  }
}
