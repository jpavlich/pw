import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonListComponent } from './persons/person-list/person-list.component';

const routes: Routes = [
  { path: 'persons/person-list', component: PersonListComponent },
  { path: '', pathMatch: 'full', redirectTo: '/persons/person-list' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
