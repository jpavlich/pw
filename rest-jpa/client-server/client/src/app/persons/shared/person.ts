import { Company } from './company';
export class Person {
    id: number;
    name: string;
    employer: Company;
}
