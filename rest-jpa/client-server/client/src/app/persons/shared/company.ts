import { Person } from './person';

export class Company {
    id: number;
    name: string;
    taxID: string;
}
