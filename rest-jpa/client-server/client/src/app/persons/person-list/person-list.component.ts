import { Component, OnInit } from '@angular/core';

import { Person } from '../shared/person';
import { PersonService } from '../shared/person.service';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {
  persons: Person[] = [];
  constructor(private service: PersonService) { }

  ngOnInit() {
    this.service.findAll()
      .subscribe(persons => this.persons = persons);
  }

}
